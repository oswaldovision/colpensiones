var express = require('express')
var router = express.Router()
var Web3 = require('web3')

const {schemaEmpresa} = require('./../schema/empresa')

var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8095'))

var empresaContract = web3.eth.contract([{"constant":true,"inputs":[],"name":"nit","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"nombre","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"inputs":[{"name":"_nit","type":"string"},{"name":"_nombre","type":"string"}],"payable":false,"type":"constructor"}]);

router.post('/addEmpresa', (req,res) => {
  empresaContract.new(
    req.body.nit,
    req.body.nombre,
    {
      from: web3.eth.accounts[0],
      data: '0x6060604052341561000f57600080fd5b6040516103ee3803806103ee833981016040528080518201919060200180518201919050505b816000908051906020019061004b92919061006b565b50806001908051906020019061006292919061006b565b505b5050610110565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106100ac57805160ff19168380011785556100da565b828001600101855582156100da579182015b828111156100d95782518255916020019190600101906100be565b5b5090506100e791906100eb565b5090565b61010d91905b808211156101095760008160009055506001016100f1565b5090565b90565b6102cf8061011f6000396000f30060606040526000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806319936caf14610049578063bc7b35b2146100d8575b600080fd5b341561005457600080fd5b61005c610167565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561009d5780820151818401525b602081019050610081565b50505050905090810190601f1680156100ca5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156100e357600080fd5b6100eb610205565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561012c5780820151818401525b602081019050610110565b50505050905090810190601f1680156101595780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b60008054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156101fd5780601f106101d2576101008083540402835291602001916101fd565b820191906000526020600020905b8154815290600101906020018083116101e057829003601f168201915b505050505081565b60018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561029b5780601f106102705761010080835404028352916020019161029b565b820191906000526020600020905b81548152906001019060200180831161027e57829003601f168201915b5050505050815600a165627a7a7230582009c3b864528dcf109d7ba5cd0ce4e1d5d83212bd546ed8088d53af6f43ab4c680029',
      gas: '4300000'
    }, function (e, contract){
      console.log(e, contract);
      if (typeof contract.address !== 'undefined') {
        console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);

        //save in db
        let empresa = toEmpresa(req.body)
        empresa.addressBchain = contract.address
        empresa.save(err => {
          if(!err){
            empresa.save()
            res.send(contract.address)
          }else {
            if(res){
              res.writeHead(500, { 'Content-Type': 'text/plain' });
              res.end('Error saving: ' + err);
            }
          }
        })
      }
    })
})

router.post('/getEmpresa', (req, res) => {
  var emp = empresaContract.at(req.body.address)

  let empresa = {
    nit : emp.nit(),
    nombre : emp.nombre()
  }

  res.send(empresa)
})

router.get('/getCompanies', (req,res)=>{
  schemaEmpresa.find({}, function (err, result) {
    if (err){
      console.log(err)
      return null
    }else{
      let formatedList = result.map(el => ({addressBchain: el.addressBchain, name: el.name}))
      res.setHeader('content-type','application/json');
      res.status(200).send(formatedList);
    }
  })
})

let toEmpresa = body => new schemaEmpresa({
  name : body.nombre,
  nit : body.nit,
  address : body.address,
  phone : body.phone,
  email : body.email
})

module.exports = router