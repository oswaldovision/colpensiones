const mongoose = require('mongoose')

mongoose.Promise = global.Promise
const colpensionesConnection = mongoose.createConnection(process.env.MONGODB_URI, {useMongoClient: true})
const identityConnection = mongoose.createConnection(process.env.MONGODB_IDENTITY_URI, {useMongoClient: true})

module.exports = {colpensionesConnection, identityConnection, mongoose}