const {colpensionesConnection, mongoose} = require('./../db/mongoose')
const moment = require('moment')

let PagareSchema = new mongoose.Schema({
  pensionado: {
    type: String,
    required: true,
    trim: true
  },
  empresa: {
    type: String,
    required: true,
    trim: true
  },
  propietario: {
    type: String,
    required: true,
    trim: true
  },
  crediticia: {
    type: String,
    required: true,
    trim: true
  },
  operador: {
    type: String,
    required: true,
    trim: true
  },
  fechaCreacion: {
    type: Number,
    default : moment().unix()
  },
  cupoCredito :{
    type : Number,
    default : 0
  } ,
  saldo : {
    type : Number,
    default : 0
  },
  precioInicial : {
    type : Number,
    default : 0
  },
  precioVenta : {
    type : Number,
    default : 0
  },
  addressBchain: String
})

let Pagare = colpensionesConnection.model('Pagare', PagareSchema, 'pagares')

module.exports = {schemaPagare: Pagare}