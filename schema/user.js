const {identityConnection, mongoose} = require('./../db/mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const moment = require('moment')

let UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: value => validator.isEmail(value),
      message: '{VALUE} is not a valid email'
    }
  },
  password: {
    type: String,
    require: true,
    minlength: 6
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }],
  googleId: {
    type: String
  },
  facebookId: {
    type: String
  },
  windowsLiveId: {
    type: String
  },
  twitterId: {
    type: String
  }
})

UserSchema.methods.toJSON = function () {
  let user = this
  let userObject = user.toObject()

  return {'id': userObject._id, 'email': userObject.email}
}

UserSchema.methods.generateAuthToken = function () {
  let user = this
  let access = 'auth'

  let payload = {
    _id: user._id.toHexString(),
    access,
    iat: moment().unix(),
    exp: moment().add(process.env.expirationToken, 'minutes').unix()
  }

  let token = jwt.sign(payload, process.env['JWT_SECRET']).toString()

  user.tokens.push({access, token})

  return user.save().then(() => token)
}

UserSchema.methods.removeToken = function (token) {
  let user = this

  return user.update({
    $pull: {
      tokens: {token}
    }
  })
}

UserSchema.statics.findByCredentials = function (email, password) {
  let User = this

  return User.findOne({email}).then(user => {
    if (!user)
      return Promise.reject()

    return new Promise((resolve, reject) => {
      //use bcrypt.compare to compare password argument with user.password
      bcrypt.compare(password, user.password, (err, res) => {
        if (res)
          resolve(user)
        else
          reject()
      })
    })
  })
}

UserSchema.statics.findByToken = function (token) {
  let User = this, decoded

  try{
    decoded = jwt.verify(token, process.env['JWT_SECRET'])
  }catch (e){
    return Promise.reject()
  }

  return User.findOne({
    _id : decoded._id,
    'tokens.access' : 'auth',
    'tokens.token' : token
  })
}

UserSchema.pre('save', function (next) {
  let user = this

  if (user.isModified('password')) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        user.password = hash
        next()
      })
    })
  } else {
    next()
  }
})

let User = identityConnection.model('User', UserSchema, 'users')

module.exports = {User}