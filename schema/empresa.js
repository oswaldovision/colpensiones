const {colpensionesConnection, mongoose} = require('./../db/mongoose')
const validator = require('validator')

let EmpresaSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  nit: {
    type: String,
    trim: true
  },
  address: {
    type: String
  },
  phone: {
    type: String
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: value => validator.isEmail(value),
      message: '{VALUE} is not a valid email'
    }
  },
  addressBchain: String
})

let Empresa = colpensionesConnection.model('Empresa', EmpresaSchema, 'empresas')

module.exports = {schemaEmpresa: Empresa}