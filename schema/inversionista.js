const {colpensionesConnection, mongoose} = require('./../db/mongoose')
const validator = require('validator')

let inversionistaSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  nit: {
    type: String,
    trim: true
  },
  address: {
    type: String
  },
  phone: {
    type: String
  },
  celPhone: {
    type: String
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: value => validator.isEmail(value),
      message: '{VALUE} is not a valid email'
    }
  },
  bank: {
    type: String,
    trim: true
  },
  numberBankAccount: {
    type: String,
    trim: true
  },
  addressBchain: String
})

let Inversionista = colpensionesConnection.model('Inversionista', inversionistaSchema, 'inversionistas')

module.exports = {schemaInversionista: Inversionista}