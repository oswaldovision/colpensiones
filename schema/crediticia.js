const {colpensionesConnection, mongoose} = require('./../db/mongoose')
const validator = require('validator')

const ROLES = ['Operador', 'Originador']

let CrediticiaSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  nit: {
    type: String,
    trim: true
  },
  rol: {
    type: String,
    required: true,
    enum: ROLES
  },
  address: {
    type: String
  },
  phone: {
    type: String
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: value => validator.isEmail(value),
      message: '{VALUE} is not a valid email'
    }
  },
  bank: {
    type: String,
    trim: true
  },
  numberBankAccount: {
    type: String,
    trim: true
  },
  addressBchain: String
})

let Crediticia = colpensionesConnection.model('Crediticia', CrediticiaSchema, 'crediticias')

module.exports = {schemaCrediticia: Crediticia}