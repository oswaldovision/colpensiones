const {colpensionesConnection, mongoose} = require('./../db/mongoose')
const validator = require('validator')

const TYPEDOCS = ['cc', 'rc', 'ti', 'ce', 'pas', 'asi', 'msi']

let PensionadoSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  numberDocument: {
    type: String,
    trim: true
  },
  typeDocument: {
    type: String,
    enum: TYPEDOCS
  },
  birthday: {
    type: Number,
    required: true,
    default: 1504618666
  },
  address: {
    type: String
  },
  phone: {
    type: String
  },
  celPhone: {
    type: String
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: value => validator.isEmail(value),
      message: '{VALUE} is not a valid email'
    }
  },
  bank: {
    type: String,
    trim: true
  },
  numberBankAccount: {
    type: String,
    trim: true
  },
  addressBchain: String
})

let Pensionado = colpensionesConnection.model('Pensionado', PensionadoSchema, 'pensionados')

module.exports = {schemaPensionado: Pensionado}