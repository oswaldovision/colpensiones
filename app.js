require('./config/config');
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const pagare = require('./route/pagare')
const pensionado = require('./route/pensionado')
const crediticia = require('./route/crediticia')
const empresa = require('./route/empresa')
const inversionista = require('./route/inversionista')

const port = process.env.PORT

const app = express()
app.use(bodyParser.json())
app.use(cors())

app.use('/v1/pensionado/', pensionado)
app.use('/v1/crediticia/', crediticia)
app.use('/v1/empresa/', empresa)
app.use('/v1/inversionista/', inversionista)
app.use('/v1/pagare/', pagare)

app.listen(port, () => {
  console.log(`Express server listening on port: ${port}`)
})

module.exports = {app}